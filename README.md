# What Is This?

This page is meant to be an easy cheat sheet 
for the linux command line interface (CLI).

In other words, just a reminder list of frequently used
and otherwise helpful linux commands.

For Windows/PowerShell commands help, [please check this guide](windows.md).

Hope it helps you. It definitely helped me! :)

# The Cheat Sheet

Please note that currently, the cheat sheet is oriented
towards CentOS/RHEL-based linux. Therefore some commands might differ
on Debian/Ubuntu-based distributions.

## General Usage

#### Find out which OS / distribution is running

```
cat /etc/*-release
```

#### Re-run the last command with admin rights

```
sudo !!
```

- *It's called sudo bang bang.*
- *This is a bashism (works only in Bash).*
- [More info here](https://justinsomnia.org/2006/09/sudo-bang-bang/)

#### Change the time zone

```
tzselect
```

- *Requires ntp to be installed and started.*

#### List files in current directory

```
ls -al
```

- *The -a includes files that start with a dot.*
- *The -l shows permissions and dates.*

## Security

#### Get the chmod numerical value for a file

```
stat --format '%a' <file>
```

- [More on StackExchange](https://unix.stackexchange.com/questions/46915/get-the-chmod-numerical-value-for-a-file)

#### Set file owner and group to root

```
sudo chown root:root secret.key
```

#### Set file permissions

```
sudo chmod 700 secret.key
```

- *chmod 700 means read, write, & execute only for owner.*
- [More details on linux permissions](https://en.wikipedia.org/wiki/File_system_permissions)

#### Clear the command history

```
history -cw
```

## Users

#### Create a new user

```
sudo adduser username
sudo passwd username
```

#### List local groups

```
cat /etc/group
```

#### Create local group

```
sudo groupadd mygroup
```

#### Add user to local group

```
sudo usermod -aG mygroup username
```

#### Make user admin (root)

```
sudo usermod -aG wheel username
```

- *We add the user to the system `wheel` group, which grants the user root powers.*

#### List local admins

```
sudo getent group wheel
```

#### Edit sudo configuration

```
sudo nano /etc/sudoers
```

## Updates

#### Update the YUM package cache

```
sudo yum makecache fast
```

#### Update all packages

```
sudo yum update -y
```

## Memory

#### RAM usage

```
free -h
```

## Storage

#### Extract a tar.gz

```
mkdir mydirectory
tar -xvzf myfile.tar.gz -C mydirectory
```

- [Help from Ask Ubuntu 1](https://askubuntu.com/questions/25347/what-command-do-i-need-to-unzip-extract-a-tar-gz-file)
- [Help from Ask Ubuntu 2](https://askubuntu.com/questions/45349/how-to-extract-files-to-another-directory-using-tar-command)

#### Filesystem usage

```
sudo df -h
```

- *Needs the sudo. Otherwise, it might not list all partitions.*

#### Disk usage statistics

```
sudo du -h -d 1 -x /
```

- *Needs the sudo. Otherwise, you might get read access errors.*

#### Display filesystems

```
cat /etc/fstab
```

#### Modify partitions

```
sudo fdisk /dev/sda
```

- [Help from How-To Geek](https://www.howtogeek.com/106873/how-to-use-fdisk-to-manage-partitions-on-linux/)
- [Help from Microsoft](https://blogs.msdn.microsoft.com/cloud_solution_architect/2016/05/24/step-by-step-how-to-resize-a-linux-vm-os-disk-in-azure-arm/)

#### Extend an LVM partition

```
sudo lvextend -L +5G /dev/mapper/rootvg-varlv
sudo resize2fs /dev/mapper/rootvg-varlv
sudo vgdisplay
sudo df -h
```

- *In this example, we extend the **/var** LVM partition by an extra 5 GiB.*

## Networking

#### Download a file

```
curl -O https://example.com/myfile.tar.gz
```

#### Scan network ports

`nc` or `ncat`

#### Open port in the firewall

```
sudo firewall-cmd --permanent --add-port=443/tcp
sudo firewall-cmd --reload
```

#### Reroute port using iptables

```
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 443 -j REDIRECT --to-port 8443
```

- *In this example, we're rerouting incoming traffic on port 443 to port 8443.*

#### Edit SELinux policy to allow connection to specific port

```
sudo semanage port -a -p tcp -t http_port_t 8085
sudo semanage port -l | grep 8085
```

#### Display DNS

```
cat /etc/resolv.conf
```

#### Edit network adapter settings

```
nano /etc/sysconfig/network-scripts/ifcfg-eth0
```

- *Useful to set a static IP.*

## Third-Party: Keytool

#### Create keystore from PFX certificate

```
keytool -importkeystore -srckeystore my_cert.pfx -srcstoretype PKCS12 -destkeystore keystore.jks
```

## Third-Party: Docker

#### List & remove Docker container

```
docker ps
docker rm
```

#### List & remove Docker image

```
docker images
docker rmi
```

## Third-Party: Git

#### Remove last commit

```
git reset HEAD~1 --soft
```

- *Works only with 1 commit and if the commit wasn't pushed to the server.*

# License

Please see the [LICENSE](LICENSE) file.

# Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/dmitri13" title="dmitri13">dmitri13</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
